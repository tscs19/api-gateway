package com.alro.cloud.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.discovery.DiscoveryClientRouteLocator;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

import static com.alro.cloud.utils.FilterConstants.PRE_FILTER_TYPE;

public class BlockFilter extends ZuulFilter {

    private static final String TEXT_HTML_CONTENT_TYPE = "text/html";
    private static final String EUREKA_ROUTE = "/eureka";
    private static final String EMPTY_STRING = "";
    private static final String REQUEST_URI_KEY = "requestURI";

    @Value("${zuul.permit.access}")
    private String[] accessEnabledPaths;

    private final DiscoveryClientRouteLocator routeLocator;

    private final Logger log;

    public BlockFilter(DiscoveryClientRouteLocator routeLocator, Logger logger) {
        this.routeLocator = routeLocator;
        this.log = logger;
    }

    @Override
    public String filterType() {
        return PRE_FILTER_TYPE;
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();

        String requestPath = URI.create(request.getRequestURI()).getPath();
        log.info("API Gateway: request path = {}", requestPath);

        Route route = routeLocator.getMatchingRoute(requestPath);
        /*if (route == null || !ArrayUtils.contains(accessEnabledPaths, requestPath)) {
            return notFoundResponse(requestContext);
        }*/
        return null;
    }

    private Object notFoundResponse(RequestContext requestContext) {
        requestContext.unset();
        requestContext.getResponse().setContentType(TEXT_HTML_CONTENT_TYPE);
        requestContext.setResponseStatusCode(HttpStatus.FORBIDDEN.value());
        return null;
    }
}
