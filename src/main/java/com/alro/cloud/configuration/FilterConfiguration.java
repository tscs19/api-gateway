package com.alro.cloud.configuration;

import com.alro.cloud.filter.BlockFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.discovery.DiscoveryClientRouteLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class FilterConfiguration {

    @Bean
    public BlockFilter blockFilter(DiscoveryClientRouteLocator routeLocator) {
        return new BlockFilter(routeLocator, log);
    }
}
